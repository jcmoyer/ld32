function love.conf(t)
  t.window.title = "Rocket Rabbit"
  t.window.width = 1024
  t.window.height = 768
  t.window.fullscreen = false
  t.window.vsync = false
end

